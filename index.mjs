class Bike {
  constructor(brand, speed) {
    this.brand = brand;
    this.speed = speed;
    console.info(`brand: ${this.brand}, speed: ${this.speed}`);
  }
}

const bike = new Bike("polygon", 5);
